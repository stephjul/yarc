# YARC - Yet Another Rate Converter

## Why another rate converter?
Rate converter apps generally use only one rate converter service. Sometimes, the currency needed is not present and the app becomes useless. Sometimes, user may get frustrated because the rate converter service is not the one he really needs.

The idea behind YARC is to have and Android app that can use any rate converter service. All we need to do is provide a class that can fetch and parse the data for a particular service.

For now, YARC supports only three rate converter services:
- [European Central Bank](https://www.ecb.europa.eu/stats/policy_and_exchange_rates/euro_reference_exchange_rates/html/index.en.html)
- [Exchange Rate Api](http://exchangeratesapi.io/)
- [Fixer IO](https://fixer.io/)

But any new service can be really easily added!

## List of supported currencies
YARC embeds almost *all* official currency iso codes listed on [wikipedia](https://en.wikipedia.org/wiki/ISO_4217#Active_codes), but some may be missing, so please open an issue if you found a missing one.

## What about crypto-currencies?
For now, I didn't add support for crypto-currencies, but it is very simple to add them, and I will definitely do it.. Eventually. If you really need it, let me know!

## Usage
### Rate converter service selection
On the main screen, click on the **settings** icon, to enter settings screen.
From the settings screen, you can configure several options such as the rate converter to use, how to sync rates and the theme of the app

![Settings screen](/screenshots/settings.png)

Click on the **Rate Converter** item to select the rate converter service you want to use

![Choose your rate converter service](/screenshots/rate_converter_selection.png)

You can then click on the **setting** button bellow, to configure the defined rate converter

![Rate converter setting](/screenshots/rate_converter_setting.png)

On this screen, you have different information about the rate converter you selected. If needed, you will be able to set your personal token. See section [Configure rate converter service](#configure-rate-converter-service)

### Currency selection
On the main screen, click on the **currency selection** (first icon), to open the currency selection screen

![Currency selection](/screenshots/currency_selection.png)

You can select the currency you want. No limit. The number of available currencies depends on the rate converter service you use. You can search currencies by typing the iso code, the country name, or the currency name itself!

![Search by country](/screenshots/search_by_country.png)
![Search by iso code](/screenshots/search_by_iso_code.png)

You can also click on the flag of a currency to see the countries that use this currency

![Country using the AED currency](/screenshots/countries.png)

### Converting currencies
Your selected currencies are displayed on the main screen, in the order you selected them. The first currency, at the top, is the base currency for conversion. You can use the keyboard to start the conversion immediately! If you want another currency to be the base, just click on the flag and it will switch with the first one!

![Conversion](/screenshots/conversion.png)
![Invert conversion](/screenshots/invert_conversion.png)

### Update rates
In the Settings screen, you can specify if you want to update rates at each startup or manually and if update are allowed only on WiFi or not. This may be useful when traveling! Rates are stored, to be used offline

![Synchronization options](/screenshots/sync_options.png)

You can always manually sync the rates by using the dedicated button on the main screen

## Supported features
### Theme
YARC supports light and dark theme, based on system value. A black mode is also available

![Black](/screenshots/main_screen.png)
![Dark](/screenshots/main_screen_dark.png)
![Light](/screenshots/main_screen_light.png)

### Languages
YARC supports currently only English and French languages depending on your phone settings. Any contributions to add languages are more than welcome!

## Configuring rate converter service
### European Central Bank
You don't need to do any configuration to use this service, but there is a limited number of rates available.

### Fixer IO an Exchange Rate Api services
If you need currencies that are not available with European Central Bank, you can use either Fixer IO or Exchange Rate api services. They both use a lot more currencies, but to use the services you need to register yourself on their website and claim for a user token. You can register for a free token, with some restrictions, but you can also register for a non free one, with more options. All options are not supported by YARC. When you have your token, you can enter it in  the setting screen.

## Adding new rate converter services
If you need another rate converter service, you can ask me, and I will try to implement it. But you can also contribute! YARC is free software and I will be very happy to merge contributions!

If you consider adding a new rate converter service, please see the following merge request that adds support for [Fixer IO](https://framagit.org/stephjul/yarc/-/merge_requests/1)

## Adding new translations
You want YARC to be more famous? Help me translate it in all languages in the world! If you consider adding a translation, please see the following merge request that adds [french support](https://framagit.org/stephjul/yarc/-/merge_requests/2)

# Support Me!
You can help me with YARC, by contributing, or by buying me coffee for long nights of dev :). Here is my [paypal account](https://paypal.me/stephjul84)

# Confidentiality
YARC doesn't collect any personal informations except your personal token for current rate service that need one. The token is stored in persistent data.

This app is open source, feel free to look at it!
