/*
    Copyright 2020 Julien STEPHAN (stephan-julien@laposte.net)

    This file is part of YARC.

    YARC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARC. If not, see <https://www.gnu.org/licenses/>

 */

package com.jstephan.yarc;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;


public class CurrencySelectorAdapter extends RecyclerView.Adapter<CurrencySelectorAdapter.MyViewHolder> implements Filterable {
    private List<Currency> mCurrencies;
    private List<Currency>  mFilteredCurrencies;
    private List<String> mFavorites;
    private Context context;

    // Provide a suitable constructor (depends on the kind of dataset)
    CurrencySelectorAdapter(Context context, List<Currency> currencies, List<String> favorites) {
        this.mCurrencies = currencies;
        this.mFilteredCurrencies = currencies;
        this.context = context;
        this.mFavorites = favorites;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView isoCode;
        CheckBox checkBox;
        ImageView flag;
        public TextView name;
        MyViewHolder(View v) {
            super(v);
            isoCode =  v.findViewById(R.id.currency_selection_iso_code);
            checkBox = v.findViewById(R.id.currency_selection_checkBox);
            flag = v.findViewById(R.id.currency_selection_flag);
            name = v.findViewById(R.id.currency_selection_name);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String currency = isoCode.getText().toString();
                    for (Currency entry : mFilteredCurrencies) {
                        if (entry.getIsoCode().equals(currency)) {
                            if (mFavorites.indexOf(entry.getIsoCode()) != -1) {
                                checkBox.setChecked(false);
                                mFavorites.remove(entry.getIsoCode());
                            } else {
                                checkBox.setChecked(true);
                                mFavorites.add(entry.getIsoCode());
                            }
                            break;
                        }
                    }
                }
            });
            flag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String currency = isoCode.getText().toString();
                    for (Currency entry : mFilteredCurrencies) {
                        if (entry.getIsoCode().equals(currency)) {
                            AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setTitle(currency)
                                    .setIcon(entry.getDrawable())
                                    .setMessage(context.getString(entry.getMetaData()).replace(",","\n"))
                                    .setPositiveButton(R.string.close, null)
                                    .create();
                            dialog.show();
                            break;
                        }
                    }

                }
            });
        }
    }

    @NonNull
    @Override
    public CurrencySelectorAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.currency_selection_item, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Currency currency = mFilteredCurrencies.get(position);
        holder.isoCode.setText(currency.getIsoCode());
        holder.name.setText(currency.getName());
        if (mFavorites.indexOf(currency.getIsoCode()) != -1) {
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);
        }
        holder.flag.setImageDrawable(context.getDrawable(currency.getDrawable()));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mFilteredCurrencies.size();
    }

    String getFavorites() {
        StringBuilder stringBuilder = new StringBuilder();
        String favorites;
        for (String s: mFavorites) {
            stringBuilder.append(s).append(",");
        }

        if (stringBuilder.length() > 1){
            favorites = stringBuilder.substring(0, stringBuilder.length()-1);
        } else {
            favorites = stringBuilder.toString();
        }
        return favorites;
    }

    void unselectAll() {
        mFavorites.clear();
        notifyDataSetChanged();
    }

    void selectAll() {
        for (Currency currency : mCurrencies) {
            mFavorites.add(currency.getIsoCode());
        }
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {

                    mFilteredCurrencies = mCurrencies;
                } else {
                    List<Currency> filteredList = new ArrayList<>();
                    for (Currency entry : mCurrencies) {
                        if (stripAccents(entry.getIsoCode()).toLowerCase().contains(stripAccents(charString).toLowerCase()) ||
                                stripAccents(context.getResources().getString(entry.getName())).toLowerCase().contains(stripAccents(charString).toLowerCase()) ||
                                stripAccents(context.getResources().getString(entry.getMetaData())).toLowerCase().contains(stripAccents(charString).toLowerCase())) {
                            filteredList.add(entry);
                        }
                    }

                    mFilteredCurrencies = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredCurrencies;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredCurrencies = (List<Currency>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    private static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFKD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }
}