/*
    Copyright 2020 Julien STEPHAN (stephan-julien@laposte.net)

    This file is part of YARC.

    YARC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARC. If not, see <https://www.gnu.org/licenses/>

 */

package com.jstephan.yarc;

import java.util.TreeSet;

public abstract class RateConverter {
    final static int SUCCESS = 0;
    final static int ERROR = -1;

    /**
     * Set the user token
     *
     * This function must me implemented if a user token is required
     *
     * @param userToken The user token
     */
    public void setToken(String userToken) {}

    /**
     * @return True if API need user token, false otherwise
     *
     */
    public abstract boolean requireToken();

    /**
     * @return The API url
     */
    public abstract String getApiUrl();

    /**
     *
     * @return The date of the last update
     */
    public abstract String getLastUpdateDate();

    /**
     * @return The name of the API
     */
    public abstract String getName();

    /**
     * @return Web site url
     */
    public abstract String getWebsite();

    /**
     * @return The sorted list of supported currencies
     *
     * @see Currency
     */
    public abstract TreeSet<Currency> getCurrencies();

    /**
     * The function to fetch rates from the API
     *
     * This function must be called from an asyncTask or equivalent mechanism, so interface
     * implementation do not need to handle this
     *
     * In case of error, this function MUST set an appropriate getErrorMessage that can be retrieved
     * using getErrorMessage()
     *
     * @see #getErrorMessage()
     *
     * @return 0 on success, fetch result code otherwise
     *
     */
    public abstract Integer fetchRates();

    /**
     * @return empty string if fetchRates succeed, the corresponding error message otherwise
     *
     * @see #fetchRates()
     *
     */
    public abstract String getErrorMessage();
}
