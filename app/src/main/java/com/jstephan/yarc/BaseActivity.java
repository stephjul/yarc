/*
    Copyright 2020 Julien STEPHAN (stephan-julien@laposte.net)

    This file is part of YARC.

    YARC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARC. If not, see <https://www.gnu.org/licenses/>

 */

package com.jstephan.yarc;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

class BaseActivity extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    public BaseActivity() {}

    private SharedPreferences mSharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        applyTheme();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    void applyTheme() {
        String prefTheme = mSharedPreferences.getString(getString(R.string.pref_theme), getString(R.string.theme_system_default));
        boolean prefForceBlack = mSharedPreferences.getBoolean(getString(R.string.pref_force_black), false);
        assert prefTheme != null;
        int mode;
        if (prefTheme.equalsIgnoreCase(getString(R.string.theme_dark))) {
            mode = AppCompatDelegate.MODE_NIGHT_YES;
            if (prefForceBlack)
                setTheme(R.style.AppTheme_Black);
            else
                setTheme(R.style.AppTheme);
        } else if (prefTheme.equalsIgnoreCase(getString(R.string.theme_light))) {
            mode = AppCompatDelegate.MODE_NIGHT_NO;
        } else {
            mode = AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM;
            int uiConfiguration = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
            if (uiConfiguration == Configuration.UI_MODE_NIGHT_YES) {
                if (prefForceBlack)
                    setTheme(R.style.AppTheme_Black);
                else
                    setTheme(R.style.AppTheme);
            }
        }
        AppCompatDelegate.setDefaultNightMode(mode);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.pref_theme))) {
            applyTheme();
        } else if (key.equals(getString(R.string.pref_force_black))) {
            int uiConfiguration = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
            switch (uiConfiguration) {
                case Configuration.UI_MODE_NIGHT_NO:
                    // Night mode is not active, we're using the light theme
                    break;
                case Configuration.UI_MODE_NIGHT_YES:
                    recreate();
                    // Night mode is active, we're using dark theme
                    break;
            }
            recreate();
        }
    }
}
