/*
    Copyright 2020 Julien STEPHAN (stephan-julien@laposte.net)

    This file is part of YARC.

    YARC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARC. If not, see <https://www.gnu.org/licenses/>

 */

package com.jstephan.yarc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.TreeSet;

public class MainActivity extends BaseActivity
        implements CurrencyRateConverter.onRateUpdateListener {
    private SharedPreferences mSharedPreferences;
    private ConnectivityManager mConnectivityManager;
    private Boolean mSyncWifiOnly;
    private String mRateConverter;
    private static TreeSet<Currency> mAvailableCurrencies;
    private static MainAdapter mAdapter;

    public MainActivity() {
        super();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set the decimal separator in the keypad
        TextView decimalSeparator = findViewById(R.id.buttonDecimalSeparator);
        DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance();
        DecimalFormatSymbols symbols=format.getDecimalFormatSymbols();
        decimalSeparator.setText(String.valueOf(symbols.getDecimalSeparator()));

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);
        mRateConverter = mSharedPreferences.getString(getString(R.string.pref_rate_converter), getString(R.string.default_rate_converter));
        mConnectivityManager = new ConnectivityManager(getApplicationContext());
        mSyncWifiOnly = mSharedPreferences.getBoolean(getString(R.string.pref_sync_wifi_only), true);
        String mSyncWifiMode = mSharedPreferences.getString(getString(R.string.pref_sync_mode), getString(R.string.default_sync_mode));
        assert mSyncWifiMode != null;
        if (mSyncWifiMode.compareTo(getString(R.string.sync_mode_at_startup)) == 0)
            syncRates();
        else
            loadSavedRates();

        RecyclerView mRecyclerView = findViewById(R.id.mainRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
               RecyclerView.VERTICAL);
        mAdapter = new MainAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        String favoritesCurrencies = mSharedPreferences.getString(getString(R.string.pref_favorite_currencies), "");
        assert favoritesCurrencies != null;
        for (String isoCode : favoritesCurrencies.split(",")) {
            Currency currency = Currency.isoCodeToCurrency(isoCode);
            if (currency != null) {
                mAdapter.addCurrency(currency);
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    void loadSavedRates() {
        mSharedPreferences = androidx.preference.PreferenceManager.getDefaultSharedPreferences(this);
        String availableCurrencies = mSharedPreferences.getString(getString(R.string.pref_available_currencies), null);
        if (mAvailableCurrencies == null)
            mAvailableCurrencies = new TreeSet<>();
        if (availableCurrencies != null) {
            for (String s: availableCurrencies.split(",")) {
                Currency newElement = Currency.isoCodeToCurrency(s.split(":")[0]);
                newElement.setRate(s.split(":")[1]);
                mAvailableCurrencies.add(newElement);
            }
        }
    }

    void syncRates() {
        if (mSyncWifiOnly && !mConnectivityManager.isWifiConnected()) {
            Toast toast = Toast.makeText(this, getResources().getString(R.string.error_wifi_not_connected), Toast.LENGTH_LONG);
            toast.show();
            return;
        } else if (!mConnectivityManager.isConnected()) {
            Toast toast = Toast.makeText(this, getResources().getString(R.string.error_no_connection), Toast.LENGTH_LONG);
            toast.show();
            return;
        }

        CurrencyRateConverter currencyRateConverter = new CurrencyRateConverter(this, mRateConverter);
        currencyRateConverter.registerOnFinish(this);
        currencyRateConverter.execute();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.pref_rate_converter))) {
            mRateConverter = mSharedPreferences.getString(key, getResources().getStringArray(R.array.available_rate_converter)[0]);
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.remove(getString(R.string.pref_available_currencies));
            editor.remove(getString(R.string.pref_favorite_currencies));
            editor.remove(getString(R.string.pref_token));
            editor.apply();
            syncRates();
        } else if (key.equals(getString(R.string.pref_sync_wifi_only))) {
            mSyncWifiOnly = !mSyncWifiOnly;
        } else if (key.equals(getString(R.string.pref_favorite_currencies))) {
            String fetch = sharedPreferences.getString(key, null);
            mAdapter.clearData();
            if (fetch != null) {
                for (String s : fetch.split(",")) {
                    Currency currency = Currency.isoCodeToCurrency(s);
                    if (currency != null)
                        mAdapter.addCurrency(Currency.isoCodeToCurrency(s));
                }
            }
            mAdapter.refreshValues();
        } else if (key.equalsIgnoreCase(getString(R.string.pref_token))) {
            syncRates();
        } else {
            super.onSharedPreferenceChanged(sharedPreferences, key);
        }
    }

    @Override
    public void onRateUpdated(int status, TreeSet<Currency> availableCurrencies) {
        mAvailableCurrencies = availableCurrencies;
        mAdapter.refreshValues();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            return true;
        } else if (id == R.id.action_sync) {
            syncRates();
            return true;
        } else if (id == R.id.action_selection) {
            startActivity(new Intent(MainActivity.this, CurrencySelector.class));
        }
        return super.onOptionsItemSelected(item);
    }

    public static void onKeypadClick(View v) {
        mAdapter.updateValues(v);
    }
}
