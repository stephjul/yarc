/*
    Copyright 2020 Julien STEPHAN (stephan-julien@laposte.net)

    This file is part of YARC.

    YARC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARC. If not, see <https://www.gnu.org/licenses/>

 */

package com.jstephan.yarc;

enum Currency {
    AED ("AED", R.string.AED, R.drawable.aed, R.string.AED_metadata, "0", "0"),
    AFN ("AFN", R.string.AFN, R.drawable.afn, R.string.AFN_metadata, "0", "0"),
    ALL ("ALL", R.string.ALL, R.drawable.all, R.string.ALL_metadata, "0", "0"),
    AMD ("AMD", R.string.AMD, R.drawable.amd, R.string.AMD_metadata, "0", "0"),
    ANG ("ANG", R.string.ANG, R.drawable.ang, R.string.ANG_metadata, "0", "0"),
    AOA ("AOA", R.string.AOA, R.drawable.aoa, R.string.AOA_metadata, "0", "0"),
    ARS ("ARS", R.string.ARS, R.drawable.ars, R.string.ARS_metadata, "0", "0"),
    AUD ("AUD", R.string.AUD, R.drawable.aud, R.string.AUD_metadata, "0", "0"),
    AWG ("AWG", R.string.AWG, R.drawable.awg, R.string.AWG_metadata, "0", "0"),
    AZN ("AZN", R.string.AZN, R.drawable.azn, R.string.AZN_metadata, "0", "0"),
    BAM ("BAM", R.string.BAM, R.drawable.bam, R.string.BAM_metadata, "0", "0"),
    BBD ("BBD", R.string.BBD, R.drawable.bbd, R.string.BBD_metadata, "0", "0"),
    BDT ("BDT", R.string.BDT, R.drawable.bdt, R.string.BDT_metadata, "0", "0"),
    BGN ("BGN", R.string.BGN, R.drawable.bgn, R.string.BGN_metadata, "0", "0"),
    BHD ("BHD", R.string.BHD, R.drawable.bhd, R.string.BHD_metadata, "0", "0"),
    BIF ("BIF", R.string.BIF, R.drawable.bif, R.string.BIF_metadata, "0", "0"),
    BMD ("BMD", R.string.BMD, R.drawable.bmd, R.string.BMD_metadata, "0", "0"),
    BND ("BND", R.string.BND, R.drawable.bnd, R.string.BND_metadata, "0", "0"),
    BOB ("BOB", R.string.BOB, R.drawable.bob, R.string.BOB_metadata, "0", "0"),
    BRL ("BRL", R.string.BRL, R.drawable.brl, R.string.BRL_metadata, "0", "0"),
    BSD ("BSD", R.string.BSD, R.drawable.bsd, R.string.BSD_metadata, "0", "0"),
    BTN ("BTN", R.string.BTN, R.drawable.btn, R.string.BTN_metadata, "0", "0"),
    BWP ("BWP", R.string.BWP, R.drawable.bwp, R.string.BWP_metadata, "0", "0"),
    BYN ("BYN", R.string.BYN, R.drawable.byn, R.string.BYN_metadata, "0", "0"),
    BZD ("BZD", R.string.BZD, R.drawable.bzd, R.string.BZD_metadata, "0", "0"),
    CAD ("CAD", R.string.CAD, R.drawable.cad, R.string.CAD_metadata, "0", "0"),
    CDF ("CDF", R.string.CDF, R.drawable.cdf, R.string.CDF_metadata, "0", "0"),
    CHF ("CHF", R.string.CHF, R.drawable.chf, R.string.CHF_metadata, "0", "0"),
    CLP ("CLP", R.string.CLP, R.drawable.clp, R.string.CLP_metadata, "0", "0"),
    CNY ("CNY", R.string.CNY, R.drawable.cny, R.string.CNY_metadata, "0", "0"),
    COP ("COP", R.string.COP, R.drawable.cop, R.string.COP_metadata, "0", "0"),
    CRC ("CRC", R.string.CRC, R.drawable.crc, R.string.CRC_metadata, "0", "0"),
    CUP ("CUP", R.string.CUP, R.drawable.cup, R.string.CUP_metadata, "0", "0"),
    CVE ("CVE", R.string.CVE, R.drawable.cve, R.string.CVE_metadata, "0", "0"),
    CZK ("CZK", R.string.CZK, R.drawable.czk, R.string.CZK_metadata, "0", "0"),
    DJF ("DJF", R.string.DJF, R.drawable.djf, R.string.DJF_metadata, "0", "0"),
    DKK ("DKK", R.string.DKK, R.drawable.dkk, R.string.DKK_metadata, "0", "0"),
    DOP ("DOP", R.string.DOP, R.drawable.dop, R.string.DOP_metadata, "0", "0"),
    DZD ("DZD", R.string.DZD, R.drawable.dzd, R.string.DZD_metadata, "0", "0"),
    EGP ("EGP", R.string.EGP, R.drawable.egp, R.string.EGP_metadata, "0", "0"),
    ERN ("ERN", R.string.ERN, R.drawable.ern, R.string.ERN_metadata, "0", "0"),
    ETB ("ETB", R.string.ETB, R.drawable.etb, R.string.ETB_metadata, "0", "0"),
    EUR ("EUR", R.string.EUR, R.drawable.eur, R.string.EUR_metadata, "0", "0"),
    FJD ("FJD", R.string.FJD, R.drawable.fjd, R.string.FJD_metadata, "0", "0"),
    FKP ("FKP", R.string.FKP, R.drawable.fkp, R.string.FKP_metadata, "0", "0"),
    GBP ("GBP", R.string.GBP, R.drawable.gbp, R.string.GBP_metadata, "0", "0"),
    GEL ("GEL", R.string.GEL, R.drawable.gel, R.string.GEL_metadata, "0", "0"),
    GHS ("GHS", R.string.GHS, R.drawable.ghs, R.string.GHS_metadata, "0", "0"),
    GIP ("GIP", R.string.GIP, R.drawable.gip, R.string.GIP_metadata, "0", "0"),
    GMD ("GMD", R.string.GMD, R.drawable.gmd, R.string.GMD_metadata, "0", "0"),
    GNF ("GNF", R.string.GNF, R.drawable.gnf, R.string.GNF_metadata, "0", "0"),
    GTQ ("GTQ", R.string.GTQ, R.drawable.gtq, R.string.GTQ_metadata, "0", "0"),
    GYD ("GYD", R.string.GYD, R.drawable.gyd, R.string.GYD_metadata, "0", "0"),
    HKD ("HKD", R.string.HKD, R.drawable.hkd, R.string.HKD_metadata, "0", "0"),
    HNL ("HNL", R.string.HNL, R.drawable.hnl, R.string.HNL_metadata, "0", "0"),
    HRK ("HRK", R.string.HRK, R.drawable.hrk, R.string.HRK_metadata, "0", "0"),
    HTG ("HTG", R.string.HTG, R.drawable.htg, R.string.HTG_metadata, "0", "0"),
    HUF ("HUF", R.string.HUF, R.drawable.huf, R.string.HUF_metadata, "0", "0"),
    IDR ("IDR", R.string.IDR, R.drawable.idr, R.string.IDR_metadata, "0", "0"),
    ILS ("ILS", R.string.ILS, R.drawable.ils, R.string.ILS_metadata, "0", "0"),
    INR ("INR", R.string.INR, R.drawable.inr, R.string.INR_metadata, "0", "0"),
    IQD ("IQD", R.string.IQD, R.drawable.iqd, R.string.IQD_metadata, "0", "0"),
    IRR ("IRR", R.string.IRR, R.drawable.irr, R.string.IRR_metadata, "0", "0"),
    ISK ("ISK", R.string.ISK, R.drawable.isk, R.string.ISK_metadata, "0", "0"),
    JMD ("JMD", R.string.JMD, R.drawable.jmd, R.string.JMD_metadata, "0", "0"),
    JOD ("JOD", R.string.JOD, R.drawable.jod, R.string.JOD_metadata, "0", "0"),
    JPY ("JPY", R.string.JPY, R.drawable.jpy, R.string.JPY_metadata, "0", "0"),
    KES ("KES", R.string.KES, R.drawable.kes, R.string.KES_metadata, "0", "0"),
    KGS ("KGS", R.string.KGS, R.drawable.kgs, R.string.KGS_metadata, "0", "0"),
    KHR ("KHR", R.string.KHR, R.drawable.khr, R.string.KHR_metadata, "0", "0"),
    KMF ("KMF", R.string.KMF, R.drawable.kmf, R.string.KMF_metadata, "0", "0"),
    KPW ("KPW", R.string.KPW, R.drawable.kpw, R.string.KPW_metadata, "0", "0"),
    KRW ("KRW", R.string.KRW, R.drawable.krw, R.string.KRW_metadata, "0", "0"),
    KWD ("KWD", R.string.KWD, R.drawable.kwd, R.string.KWD_metadata, "0", "0"),
    KYD ("KYD", R.string.KYD, R.drawable.kyd, R.string.KYD_metadata, "0", "0"),
    KZT ("KZT", R.string.KZT, R.drawable.kzt, R.string.KZT_metadata, "0", "0"),
    LAK ("LAK", R.string.LAK, R.drawable.lak, R.string.LAK_metadata, "0", "0"),
    LBP ("LBP", R.string.LBP, R.drawable.lbp, R.string.LBP_metadata, "0", "0"),
    LKR ("LKR", R.string.LKR, R.drawable.lkr, R.string.LKR_metadata, "0", "0"),
    LRD ("LRD", R.string.LRD, R.drawable.lrd, R.string.LRD_metadata, "0", "0"),
    LSL ("LSL", R.string.LSL, R.drawable.lsl, R.string.LSL_metadata, "0", "0"),
    LYD ("LYD", R.string.LYD, R.drawable.lyd, R.string.LYD_metadata, "0", "0"),
    MAD ("MAD", R.string.MAD, R.drawable.mad, R.string.MAD_metadata, "0", "0"),
    MDL ("MDL", R.string.MDL, R.drawable.mdl, R.string.MDL_metadata, "0", "0"),
    MGA ("MGA", R.string.MGA, R.drawable.mga, R.string.MGA_metadata, "0", "0"),
    MKD ("MKD", R.string.MKD, R.drawable.mkd, R.string.MKD_metadata, "0", "0"),
    MMK ("MMK", R.string.MMK, R.drawable.mmk, R.string.MMK_metadata, "0", "0"),
    MNT ("MNT", R.string.MNT, R.drawable.mnt, R.string.MNT_metadata, "0", "0"),
    MOP ("MOP", R.string.MOP, R.drawable.mop, R.string.MOP_metadata, "0", "0"),
    MRU ("MRU", R.string.MRU, R.drawable.mru, R.string.MRO_metadata, "0", "0"),
    MUR ("MUR", R.string.MUR, R.drawable.mur, R.string.MUR_metadata, "0", "0"),
    MVR ("MVR", R.string.MVR, R.drawable.mvr, R.string.MVR_metadata, "0", "0"),
    MWK ("MWK", R.string.MWK, R.drawable.mwk, R.string.MWK_metadata, "0", "0"),
    MXN ("MXN", R.string.MXN, R.drawable.mxn, R.string.MXN_metadata, "0", "0"),
    MYR ("MYR", R.string.MYR, R.drawable.myr, R.string.MYR_metadata, "0", "0"),
    MZN ("MZN", R.string.MZN, R.drawable.mzn, R.string.MZN_metadata, "0", "0"),
    NAD ("NAD", R.string.NAD, R.drawable.nad, R.string.NAD_metadata, "0", "0"),
    NGN ("NGN", R.string.NGN, R.drawable.ngn, R.string.NGN_metadata, "0", "0"),
    NIO ("NIO", R.string.NIO, R.drawable.nio, R.string.NIO_metadata, "0", "0"),
    NOK ("NOK", R.string.NOK, R.drawable.nok, R.string.NOK_metadata, "0", "0"),
    NPR ("NPR", R.string.NPR, R.drawable.npr, R.string.NPR_metadata, "0", "0"),
    NZD ("NZD", R.string.NZD, R.drawable.nzd, R.string.NZD_metadata, "0", "0"),
    OMR ("OMR", R.string.OMR, R.drawable.omr, R.string.OMR_metadata, "0", "0"),
    PAB ("PAB", R.string.PAB, R.drawable.pab, R.string.PAB_metadata, "0", "0"),
    PEN ("PEN", R.string.PEN, R.drawable.pen, R.string.PEN_metadata, "0", "0"),
    PGK ("PGK", R.string.PGK, R.drawable.pgk, R.string.PGK_metadata, "0", "0"),
    PHP ("PHP", R.string.PHP, R.drawable.php, R.string.PHP_metadata, "0", "0"),
    PKR ("PKR", R.string.PKR, R.drawable.pkr, R.string.PKR_metadata, "0", "0"),
    PLN ("PLN", R.string.PLN, R.drawable.pln, R.string.PLN_metadata, "0", "0"),
    PYG ("PYG", R.string.PYG, R.drawable.pyg, R.string.PYG_metadata, "0", "0"),
    QAR ("QAR", R.string.QAR, R.drawable.qar, R.string.QAR_metadata, "0", "0"),
    RON ("RON", R.string.RON, R.drawable.ron, R.string.RON_metadata, "0", "0"),
    RSD ("RSD", R.string.RSD, R.drawable.rsd, R.string.RSD_metadata, "0", "0"),
    RUB ("RUB", R.string.RUB, R.drawable.rub, R.string.RUB_metadata, "0", "0"),
    RWF ("RWF", R.string.RWF, R.drawable.rwf, R.string.RWF_metadata, "0", "0"),
    SAR ("SAR", R.string.SAR, R.drawable.sar, R.string.SAR_metadata, "0", "0"),
    SBD ("SBD", R.string.SBD, R.drawable.sbd, R.string.SBD_metadata, "0", "0"),
    SCR ("SCR", R.string.SCR, R.drawable.scr, R.string.SCR_metadata, "0", "0"),
    SDG ("SDG", R.string.SDG, R.drawable.sdg, R.string.SDG_metadata, "0", "0"),
    SEK ("SEK", R.string.SEK, R.drawable.sek, R.string.SEK_metadata, "0", "0"),
    SGD ("SGD", R.string.SGD, R.drawable.sgd, R.string.SGD_metadata, "0", "0"),
    SHP ("SHP", R.string.SHP, R.drawable.shp, R.string.SHP_metadata, "0", "0"),
    SLL ("SLL", R.string.SLL, R.drawable.sll, R.string.SLL_metadata, "0", "0"),
    SOS ("SOS", R.string.SOS, R.drawable.sos, R.string.SOS_metadata, "0", "0"),
    SRD ("SRD", R.string.SRD, R.drawable.srd, R.string.SRD_metadata, "0", "0"),
    SSP ("SSP", R.string.SSP, R.drawable.ssp, R.string.SSP_metadata, "0", "0"),
    STN ("STN", R.string.STN, R.drawable.stn, R.string.STN_metadata, "0", "0"),
    SVC ("SVC", R.string.SVC, R.drawable.svc, R.string.SVC_metadata, "0", "0"),
    SYP ("SYP", R.string.SYP, R.drawable.syp, R.string.SYP_metadata, "0", "0"),
    SZL ("SZL", R.string.SZL, R.drawable.szl, R.string.SZL_metadata, "0", "0"),
    THB ("THB", R.string.THB, R.drawable.thb, R.string.THB_metadata, "0", "0"),
    TJS ("TJS", R.string.TJS, R.drawable.tjs, R.string.TJS_metadata, "0", "0"),
    TMT ("TMT", R.string.TMT, R.drawable.tmt, R.string.TMT_metadata, "0", "0"),
    TND ("TND", R.string.TND, R.drawable.tnd, R.string.TND_metadata, "0", "0"),
    TOP ("TOP", R.string.TOP, R.drawable.top, R.string.TOP_metadata, "0", "0"),
    TRY ("TRY", R.string.TRY, R.drawable.tr, R.string.TRY_metadata, "0", "0"),
    TTD ("TTD", R.string.TTD, R.drawable.ttd, R.string.TTD_metadata, "0", "0"),
    TWD ("TWD", R.string.TWD, R.drawable.twd, R.string.TWD_metadata, "0", "0"),
    TZS ("TZS", R.string.TZS, R.drawable.tzs, R.string.TZS_metadata, "0", "0"),
    UAH ("UAH", R.string.UAH, R.drawable.uah, R.string.UAH_metadata, "0", "0"),
    UGX ("UGX", R.string.UGX, R.drawable.ugx, R.string.UGX_metadata, "0", "0"),
    USD ("USD", R.string.USD, R.drawable.usd, R.string.USD_metadata, "0", "0"),
    UYU ("UYU", R.string.UYU, R.drawable.uyu, R.string.UYU_metadata, "0", "0"),
    UZS ("UZS", R.string.UZS, R.drawable.uzs, R.string.UZS_metadata, "0", "0"),
    VES ("VES", R.string.VES, R.drawable.ves, R.string.VES_metadata, "0", "0"),
    VND ("VND", R.string.VND, R.drawable.vnd, R.string.VND_metadata, "0", "0"),
    VUV ("VUV", R.string.VUV, R.drawable.vuv, R.string.VUV_metadata, "0", "0"),
    WST ("WST", R.string.WST, R.drawable.wst, R.string.WST_metadata, "0", "0"),
    XAF ("XAF", R.string.XAF, R.drawable.xaf, R.string.XAF_metadata, "0", "0"),
    XCD ("XCD", R.string.XCD, R.drawable.xcd, R.string.XCD_metadata, "0", "0"),
    XOF ("XOF", R.string.XOF, R.drawable.xof, R.string.XOF_metadata, "0", "0"),
    XPF ("XPF", R.string.XPF, R.drawable.xpf, R.string.XPF_metadata, "0", "0"),
    YER ("YER", R.string.YER, R.drawable.yer, R.string.YER_metadata, "0", "0"),
    ZAR ("ZAR", R.string.ZAR, R.drawable.zar, R.string.ZAR_metadata, "0", "0"),
    ZMW ("ZMW", R.string.ZMW, R.drawable.zmw, R.string.ZMW_metadata, "0", "0"),
    ZWL ("ZWL", R.string.ZWL, R.drawable.zwl, R.string.ZWL_metadata, "0", "0");

    private String isoCode;
    private int name;
    private int drawable;
    private int metaData;
    private String rate;
    private String value;

    Currency(String isoCode, int name, int drawable, int metaData, String rate, String value){
        this.isoCode = isoCode;
        this.name = name;
        this.drawable = drawable;
        this.metaData = metaData;
        this.rate = rate;
        this.value = value;

    }


    public static Currency isoCodeToCurrency(String isoCode) {
        Currency returnValue = null;
        for (Currency currency: Currency.values()) {
            if (isoCode.equals(currency.isoCode)) {
                returnValue = currency;
                break;
            }
        }
        return returnValue;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public int getName() {
        return name;
    }

    public int getDrawable() {
        return drawable;
    }

    public int getMetaData() {
        return metaData;
    }

    /*public String toString() {
        return isoCode+","+name+","+drawable+","+ metaData;
    }
*/
    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /*public String string() {
        return this.isoCode+"."+this.drawable+"."+this.rate+"."+this.value;
    }

     */
}
