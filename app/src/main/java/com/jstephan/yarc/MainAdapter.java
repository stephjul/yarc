/*
    Copyright 2020 Julien STEPHAN (stephan-julien@laposte.net)

    This file is part of YARC.

    YARC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARC. If not, see <https://www.gnu.org/licenses/>

 */

package com.jstephan.yarc;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MyViewHolder> {
    private List<Currency> mDataset;
    private WeakReference<Context> mContext;
    private SharedPreferences mSharedPreferences;


    void addCurrency(Currency currency) {
        mDataset.add(currency);
    }

    void clearData() {
        mDataset.clear();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView isoCode;
        ImageView flag;
        TextView name;
        TextView value;
        MyViewHolder(View v) {
            super(v);
            isoCode =  v.findViewById(R.id.currency22);
            name = v.findViewById(R.id.currency23);
            value = v.findViewById(R.id.value);
            flag = v.findViewById(R.id.imageView2);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StringBuilder stringBuilder = new StringBuilder();
                    String favorites;
                    for (Currency currency: mDataset) {
                        if (currency.getIsoCode().compareToIgnoreCase(isoCode.getText().toString()) == 0 )
                            stringBuilder.insert(0,currency.getIsoCode()+",");
                        else
                            stringBuilder.append(currency.getIsoCode()).append(",");
                    }
                    if (stringBuilder.length() > 1){
                        favorites = stringBuilder.substring(0, stringBuilder.length()-1);
                    } else {
                        favorites = stringBuilder.toString();
                    }
                    SharedPreferences.Editor editor = mSharedPreferences.edit();
                    editor.putString(mContext.get().getString(R.string.pref_favorite_currencies), favorites);
                    editor.apply();
                }
            });
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    MainAdapter(Context context) {
        this.mDataset = new ArrayList<>();
        this.mContext = new WeakReference<>(context);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    void refreshValues() {
        doRefreshValues(0);
    }

    private void doRefreshValues(int index) {
        if ((mDataset.size() <= 0)) {
            notifyDataSetChanged();
            return;
        }
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        NumberFormat rateNumberFormat = NumberFormat.getNumberInstance(Locale.US);

        try {
            int fractionDigits;
            fractionDigits = 3;
            numberFormat.setMaximumFractionDigits(fractionDigits);
            double defaultValue =  numberFormat.parse(mDataset.get(0).getValue()).doubleValue();
            double defaultRate = rateNumberFormat.parse(mDataset.get(0).getRate()).doubleValue();
            for (Currency currency : mDataset.subList(index, mDataset.size())) {
                double rate;
                try {
                    rate = rateNumberFormat.parse(currency.getRate()).doubleValue();
                    currency.setValue(numberFormat.format((defaultValue/ defaultRate)* rate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            notifyDataSetChanged();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    void updateValues(View view) {
        if ((mDataset.size() <= 0)) {
            return;
        }
        TextView keypadKey = (TextView)view;
        String newValue;
        String oldValue = mDataset.get(0).getValue();
        try {
            switch (keypadKey.getId()) {
                case R.id.buttonAC:
                    newValue = "0";
                    break;
                case R.id.buttonDel:
                    if (oldValue.length()>1) {
                        newValue = oldValue.substring(0, oldValue.length()-1);
                    } else {
                        newValue = "0";
                    }
                    break;
                case R.id.buttonDecimalSeparator:
                    if (!oldValue.contains(",")){
                        mDataset.get(0).setValue(oldValue.concat(keypadKey.getText().toString()));
                    }
                    notifyDataSetChanged();
                    // refreshValues(1);
                    return;
                default:
                    if (oldValue.equals("0")){
                        newValue = keypadKey.getText().toString();
                    } else {
                        if (oldValue.contains(",") ) {
                            String[] number = oldValue.split(",");
                            if (number.length > 1 && oldValue.split(",")[1].length() >= 3){
                                newValue = oldValue;
                                break;
                            }
                        }
                        newValue = oldValue.concat(keypadKey.getText().toString());

                    }
                    break;
            }
            NumberFormat numberFormat = NumberFormat.getNumberInstance();
            mDataset.get(0).setValue(numberFormat.format(numberFormat.parse(newValue).doubleValue()));
            refreshValues();
        } catch (NullPointerException | ParseException ignored) {
        }
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MainAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_item, parent, false);
        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //
        holder.isoCode.setText(mDataset.get(position).getIsoCode());
        holder.flag.setImageDrawable(mContext.get().getDrawable(mDataset.get(position).getDrawable()));
        holder.name.setText(mDataset.get(position).getName());
        holder.value.setText(mDataset.get(position).getValue());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
