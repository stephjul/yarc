/*
    Copyright 2021 Julien STEPHAN (stephan-julien@laposte.net)

    This file is part of YARC.

    YARC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARC. If not, see <https://www.gnu.org/licenses/>

 */

package com.jstephan.yarc;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.TreeSet;

final class EuropeanCentralBank extends RateConverter {
    private final String apiURL = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
    private String lastUpdateDate;
    private String errorMessage;
    private TreeSet<Currency> currencies;

    EuropeanCentralBank() {
        currencies = new TreeSet<>();
    }

    @Override
    public boolean requireToken() {
        return false;
    }

    @Override
    public String getApiUrl() {
        return apiURL;
    }

    @Override
    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    @Override
    public String getName() {
        return "EuropeanCentralBank";
    }

    @Override
    public String getWebsite() {
        return "https://www.ecb.europa.eu/stats/policy_and_exchange_rates/euro_reference_exchange_rates/html/index.en.html";
    }

    @Override
    public TreeSet<Currency> getCurrencies() {
        return currencies;
    }

    @Override
    public Integer fetchRates() {
        URL url;
        HttpURLConnection request;

        try {
            Currency currency;
            url = new URL(apiURL);
            request = (HttpURLConnection) url.openConnection();
            request.connect();
            InputStream inputStream = (InputStream) request.getContent();

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();

            parser.setInput(inputStream, null);

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (parser.getName().equalsIgnoreCase("Cube")) {
                            if (parser.getAttributeCount() == 1) {
                                String time = parser.getAttributeValue(null, "time");
                                if (time != null) {
                                    lastUpdateDate = time;
                                }
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (parser.getName().equalsIgnoreCase("Cube")) {
                            if (parser.getAttributeCount() == 2) {
                                    String currencyIsoCode = parser.getAttributeValue(null, "currency");
                                    String rate = parser.getAttributeValue(null, "rate");
                                    if (currencyIsoCode != null && rate != null) {
                                        currency = Currency.isoCodeToCurrency(currencyIsoCode);
                                        currency.setRate(rate);
                                        currencies.add(currency);
                                    }
                            }
                        }
                        break;
                    default:
                        break;
                }
                eventType = parser.next();
            }
            if (!currencies.isEmpty()) {
                currency = Currency.isoCodeToCurrency("EUR");
                currency.setRate("1.0");
                currencies.add(currency);
            }
            request.disconnect();
        } catch (Exception e) {
            errorMessage = e.getMessage();
            return ERROR;
        }

        return SUCCESS;
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }
}
