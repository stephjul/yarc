/*
    Copyright 2020 Julien STEPHAN (stephan-julien@laposte.net)

    This file is part of YARC.

    YARC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARC. If not, see <https://www.gnu.org/licenses/>

 */

package com.jstephan.yarc;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;

class KeyPadTextView extends AppCompatTextView {
    public KeyPadTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setClickable(true);
        setFocusable(true);
        setGravity(Gravity.FILL);
        setTextAlignment(TEXT_ALIGNMENT_CENTER);
        setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.keypadTextSize));
        TypedValue outValue = new TypedValue();
        context.getTheme().resolveAttribute(
                android.R.attr.selectableItemBackgroundBorderless, outValue, true);
        setBackground(context.getDrawable(outValue.resourceId));
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.onKeypadClick(v);
            }
        });
    }
}