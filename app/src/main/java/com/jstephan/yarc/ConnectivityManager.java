/*
    Copyright 2020 Julien STEPHAN (stephan-julien@laposte.net)

    This file is part of YARC.

    YARC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARC. If not, see <https://www.gnu.org/licenses/>

 */

package com.jstephan.yarc;

import android.content.Context;
import android.net.Network;
import android.net.NetworkInfo;

class ConnectivityManager {
        private Context mContext;

        ConnectivityManager(Context context){
            mContext = context;
        }

        boolean isWifiConnected() {
            android.net.ConnectivityManager connectivityManager = (android.net.ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);


            for (Network network: connectivityManager.getAllNetworks()){
                NetworkInfo networkInfo = connectivityManager.getNetworkInfo(network);
                if (networkInfo.getTypeName().equalsIgnoreCase("WIFI") && networkInfo.isConnected())
                    return true;
            }
            return false;
        }

        boolean isConnected() {
            android.net.ConnectivityManager connectivityManager = (android.net.ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

            for (Network network : connectivityManager.getAllNetworks()) {
                NetworkInfo networkInfo = connectivityManager.getNetworkInfo(network);
                if (networkInfo.isConnected())
                    return true;
            }
            return false;
        }
}
