/*
    Copyright 2020 Julien STEPHAN (stephan-julien@laposte.net)

    This file is part of YARC.

    YARC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARC. If not, see <https://www.gnu.org/licenses/>

 */

package com.jstephan.yarc;

import android.util.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.TreeSet;

final class FixerIo extends RateConverter {
    private String apiURL = "http://data.fixer.io/api/latest?access_key=";
    private String lastUpdateDate;
    private String errorMessage;
    private TreeSet<Currency> currencies;

    FixerIo() {
        currencies = new TreeSet<>();
    }

    @Override
    public boolean requireToken() {
        return true;
    }

    @Override
    public void setToken(String userToken) {
        apiURL = apiURL.concat(userToken);
    }

    @Override
    public String getApiUrl() {
        return apiURL;
    }

    @Override
    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    @Override
    public String getName() {
        return "Fixer Io";
    }

    @Override
    public String getWebsite() {
        return "https://fixer.io/";
    }

    @Override
    public TreeSet<Currency> getCurrencies() {
        return currencies;
    }

    @Override
    public Integer fetchRates() {
        URL url;
        HttpURLConnection request;
        JsonReader jsonReader;
        boolean success = true;

        try {
            Currency currency;
            url = new URL(apiURL);
            request = (HttpURLConnection) url.openConnection();
            request.connect();
            jsonReader = new JsonReader(new InputStreamReader((InputStream) request.getContent()));

            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
                String key = jsonReader.nextName();
                switch (key) {
                    case "success":
                        success = jsonReader.nextBoolean();
                        break;
                    case "base":
                        //todo handle error
                        currency = Currency.isoCodeToCurrency(jsonReader.nextString());
                        currency.setRate("1.0");
                        currencies.add(currency);
                        break;
                    case "date":
                        lastUpdateDate = jsonReader.nextString();
                        break;
                    case "rates":
                        jsonReader.beginObject();
                        while (jsonReader.hasNext()) {
                            String name = jsonReader.nextName();
                            currency = Currency.isoCodeToCurrency(name);
                            double rate = jsonReader.nextDouble();
                            if (currency != null) {
                                currency.setRate(Double.toString(rate));
                                currencies.add(currency);
                            }
                        }
                        jsonReader.endObject();

                        break;
                    case "error":
                        jsonReader.beginObject();
                        while (jsonReader.hasNext()) {
                            String name = jsonReader.nextName();
                            if (name.equalsIgnoreCase("info")) {
                                errorMessage = jsonReader.nextString();
                            } else {
                                jsonReader.skipValue();
                            }
                        }
                        jsonReader.endObject();
                        break;
                    default:
                        jsonReader.skipValue();
                        break;
                }
            }
            jsonReader.endObject();
            jsonReader.close();
            request.disconnect();
        } catch (Exception e) {
            errorMessage = e.getMessage();
            return ERROR;
        }

        if (!success)
            return ERROR;

        return SUCCESS;

    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }
}
