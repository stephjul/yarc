/*
    Copyright 2020 Julien STEPHAN (stephan-julien@laposte.net)

    This file is part of YARC.

    YARC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARC. If not, see <https://www.gnu.org/licenses/>

 */

package com.jstephan.yarc;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

class CurrencyRateConverter extends AsyncTask<Void, Void, Integer> {

    private WeakReference<Context> mContext;
    private RateConverter abstractCurrencyConverter;
    private SharedPreferences mSharedPreferences;
    private List<onRateUpdateListener> mListeners = new ArrayList<>();

    CurrencyRateConverter(Context context, String rateConverter) {
        mContext = new WeakReference<>(context);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (rateConverter != null && rateConverter.equalsIgnoreCase(mContext.get().getString(R.string.fixer_io))) {
            abstractCurrencyConverter = new FixerIo();
        } else if (rateConverter != null && rateConverter.equalsIgnoreCase(mContext.get().getString(R.string.exchange_rate_api))) {
            abstractCurrencyConverter = new ExchangeRateApi();
        } else {
            abstractCurrencyConverter = new EuropeanCentralBank();
        }

        SharedPreferences.Editor editor =  mSharedPreferences.edit();
        if (abstractCurrencyConverter.requireToken()) {
            editor.putBoolean(mContext.get().getString(R.string.pref_require_token), true);
            String token = mSharedPreferences.getString(mContext.get().getString(R.string.pref_token), null);
            if (token != null) {
                abstractCurrencyConverter.setToken(token);
            }
        } else {
            editor.putBoolean(mContext.get().getString(R.string.pref_require_token), false);
        }
        editor.putString(mContext.get().getString(R.string.pref_rate_converter_api_url), abstractCurrencyConverter.getApiUrl());
        editor.putString(mContext.get().getString(R.string.pref_rate_converter_web_site), abstractCurrencyConverter.getWebsite());
        editor.apply();
    }

    @Override
    protected Integer doInBackground(Void... voids) {
        return abstractCurrencyConverter.fetchRates();
    }

    @Override
    protected void onPostExecute(Integer result) {
        if (result == RateConverter.SUCCESS) {
            TreeSet<Currency> availableCurrencies = abstractCurrencyConverter.getCurrencies();
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString(mContext.get().getString(R.string.pref_rate_converter_last_update_date), abstractCurrencyConverter.getLastUpdateDate());

            StringBuilder currencies = new StringBuilder();
            for (Currency entry : availableCurrencies) {
                currencies.append(entry.getIsoCode()).append(":").append(entry.getRate()).append(",");
            }
            editor.putString(mContext.get().getString(R.string.pref_available_currencies), currencies.toString());
            editor.apply();
            Toast toast = Toast.makeText(mContext.get(), mContext.get().getString(R.string.rate_update_success), Toast.LENGTH_LONG);
            toast.show();
        } else {
            Toast toast = Toast.makeText(mContext.get(), mContext.get().getString(R.string.rate_update_error) + ": " + abstractCurrencyConverter.getErrorMessage(), Toast.LENGTH_LONG);
            toast.show();
        }

        for(onRateUpdateListener listener : mListeners){
            listener.onRateUpdated(result, abstractCurrencyConverter.getCurrencies());
        }
    }

    public String getName() {
        return  abstractCurrencyConverter.getName();
    }

    void registerOnFinish(onRateUpdateListener newListener) {
         mListeners.add(newListener);
    }

    public interface onRateUpdateListener {
        void onRateUpdated(int status, TreeSet<Currency> availableCurrencies);
    }

}

