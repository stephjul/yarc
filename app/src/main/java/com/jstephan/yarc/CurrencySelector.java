/*
    Copyright 2020 Julien STEPHAN (stephan-julien@laposte.net)

    This file is part of YARC.

    YARC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with YARC. If not, see <https://www.gnu.org/licenses/>

 */

package com.jstephan.yarc;

import android.app.SearchManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CurrencySelector extends BaseActivity {

    private CurrencySelectorAdapter mAdapter;
    private SharedPreferences mSharedPreferences;

    public CurrencySelector() {
        super();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_selector);

        RecyclerView mRecyclerView = findViewById(R.id.currencySelectionRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager( new LinearLayoutManager(this));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                RecyclerView.VERTICAL);
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String availableCurrencies = mSharedPreferences.getString(getString(R.string.pref_available_currencies), null);
        String favoritesCurrencies = mSharedPreferences.getString(getString(R.string.pref_favorite_currencies), null);
        List<Currency> listAvailableCurrencies = new ArrayList<>();
        if (availableCurrencies != null) {
            for (String s: availableCurrencies.split(",")) {
                Currency newElement = Currency.isoCodeToCurrency(s.split(":")[0]);
                newElement.setRate(s.split(":")[1]);
                listAvailableCurrencies.add(newElement);

            }
        }
        List<String> listFavoritesCurrencies = new ArrayList<>();
        if (favoritesCurrencies != null) {
            listFavoritesCurrencies.addAll(Arrays.asList(favoritesCurrencies.split(",")));
        }

        mAdapter = new CurrencySelectorAdapter(this, listAvailableCurrencies, listFavoritesCurrencies);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onBackPressed() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(getString(R.string.pref_favorite_currencies), mAdapter.getFavorites());
        editor.apply();

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_currency_selector, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        assert searchManager != null;

        SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();

        assert searchView != null;

        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                mAdapter.getFilter().filter(query);
                return false;
            }

        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.findItem(R.id.select_all).setVisible(false);
                menu.findItem(R.id.select_off).setVisible(false);
                v.requestFocus();
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                menu.findItem(R.id.select_all).setVisible(true);
                menu.findItem(R.id.select_off).setVisible(true);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_search) {
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.select_off) {
            mAdapter.unselectAll();
        } else if (id == R.id.select_all) {
            mAdapter.selectAll();
        }

        return super.onOptionsItemSelected(item);
    }
}
